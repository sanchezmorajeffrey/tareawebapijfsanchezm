﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiAdventureWorks2019DSI.Models;

namespace WebApiAdventureWorks2019DSI.Data
{
    public class AdventureWorksContext: DbContext
    {
        public AdventureWorksContext(DbContextOptions<AdventureWorksContext> options) : base(options)
        {

        }
        public DbSet<ProductCategory> ProductCategory { get; set; }
    }
}
